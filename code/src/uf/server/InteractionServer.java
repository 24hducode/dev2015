package uf.server;

import com.google.gson.*;
import org.apache.http.client.methods.*;
import org.apache.http.client.*;
import org.apache.http.*;
import org.apache.http.impl.*;
import org.apache.http.impl.client.*;
import org.apache.http.entity.*;
import org.apache.http.message.*;
import org.apache.http.message.*;
import java.io.*;
import java.util.*;

import uf.server.json.*;
import utils.*;

public class InteractionServer {

    private String keyFileName;
    private KeyData keys = null;
    private Gson gson = new Gson();

    private String urlConnect = "http://24hc15.haum.org/api/connect/";
    private String baseUrl = "http://24hc15.haum.org";

    public InteractionServer(String keyFileName){
        this.keyFileName = keyFileName;
    }


    /**
     * Lit les clés (tocken, secret, name)
     */
    public void readKeys(){
        String jsonKeyData = IO.readTextFileAsString(keyFileName);
        this.keys = this.gson.fromJson(jsonKeyData, KeyData.class);
    }

    /**
     * Vérification récurente si l'action est bien un SUCCESS
     * @param res
     */
    public boolean checkSuccess(boolean res){
        if (res) {
            System.out.println("SUCCESS");
        }else{
            System.out.println("FAILURE");
        }
        return res;
    }


    /**
     * Demander un jeu
     * @param  numBot numéro du bot
     * @param  mode   training ou arana
     * @return        Réponse du serveur
     */
    public AskOut ask4Game(int numBot, String mode){
        if(keys == null) readKeys();

        // Récupère le token du bot choisi
        String tokenBot = keys.getBots().get(numBot).getToken();
        String secretBot = keys.getBots().get(numBot).getSecret();

        // Appeller l'adresse :
        /*
            POST http://24hc15.haum.org/api/connect/<bot_token>
            Classe AskIn
            {
                'secret': <bot_secret>,
                'mode': <mode>
            }
         */

        AskIn dataToSent = new AskIn();
        dataToSent.setSecret(secretBot);
        dataToSent.setMode(mode);
        dataToSent.setIncidents("false");

        // Transformation de JSON en String
        String json = this.gson.toJson(dataToSent);

        String resString = Http.postJson(urlConnect + tokenBot, json);
        AskOut rep = this.gson.fromJson(resString, AskOut.class);


        // Tester la réponse
        checkSuccess(rep.isSuccess());

        return rep;
    }


    public TestOut testStarted(String url, int numBot){

        String secretBot = keys.getBots().get(numBot).getSecret();

        /*
        {
            "secret_token": "<bot_secret>"
        }
         */
        TestIn testIn = new TestIn();
        testIn.setSecret_token(secretBot);


        // Transformation de JSON en String
        String json = this.gson.toJson(testIn);


        String resString = Http.postJson(baseUrl + url, json);
        TestOut rep = this.gson.fromJson(resString, TestOut.class);

        // Tester la réponse
        checkSuccess(rep.isSuccess());

        return rep;
    }



    /**
     * Bouger un bot
     * @param  numBot           le numéro du bot en question
     * @param  trackName        Nom de la ligne
     * @param  horaireDePassage horaire de passage
     * @param  stopId           Numéro de l'arret
     * @return                  Réponse de du server
     */
    public MoveOut move(String url, int numBot, String trackName, int stopId, String horaireDePassage){

        // url à appeler : http://24hc15.haum.org/api/<game_token>/<bot_token>/move
        String secretBot = keys.getBots().get(numBot).getSecret();

        /*
        {
            "secret_token": <bot_secret>,
            "track": <track_number>,
            "connection": datetime_str, // forme : 13 Sep 13:20:00 2015
            "to_stop": stop_id,
            "type": "move"
        }
         */

        MoveIn moveIn = new MoveIn();
        moveIn.setSecret_token(secretBot);
        moveIn.setTrack(trackName);
        moveIn.setConnection(horaireDePassage);
        moveIn.setTo_stop(stopId);
        moveIn.setType("move");

        // Transformation de JSON en String
        String json = this.gson.toJson(moveIn);
         System.out.println(json);

        String resString = Http.postJson(baseUrl + url, json);
        System.out.println(resString);
        // String resString = Http.postJson("http://localhost:8000", json);
        MoveOut rep = this.gson.fromJson(resString, MoveOut.class);

        // Tester la réponse
        	checkSuccess(rep.isSuccess());

        return rep;
    }

}