package uf.server.json;


/**
 * Json envoie informations demande jeu
 * {
 *      'secret': <bot_secret>,
 *      'mode': <mode>
 * }
 */
public class AskIn {

	private String secret;
	private String mode;
	private String incidents;

	public boolean getIncidents(){
	    return Boolean.parseBoolean(this.incidents);
	}

	public void setIncidents(String o){
	    this.incidents = o;
	}

	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String toString(){
		return "secret : " + this.secret + "; mode : " + this.mode + ";";
	}
}