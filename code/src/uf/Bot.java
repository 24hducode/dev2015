package uf;

public class Bot {

	private String date;
	private Arret arretCourant;
	private int jourSemaine;
	private int heureDepart;
	
	/**
	 * @param date
	 * @param arretCourant
	 */
	public Bot(String date, Arret arretCourant) {
		super();
		this.date = date;
		this.arretCourant = arretCourant;
		this.jourSemaine = Utils.calculerJourSemaine(this.date.split("T")[0]);
		this.heureDepart = Utils.convertIntoMinute(this.date)[1];
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
		this.jourSemaine = Utils.calculerJourSemaine(this.date);
		this.heureDepart = Utils.convertIntoMinute(this.date)[1];
	}
	public Arret getArretCourant() {
		return arretCourant;
	}
	public void setArretCourant(Arret arretCourant) {
		this.arretCourant = arretCourant;
	}
	public int getJourSemaine(){
		return this.jourSemaine;
	}
	public int getHeureDepart(){
		return this.heureDepart;
	}

	public String toString(){
		return "Bot - date : " + date + ", arretCourant : " + arretCourant +";";
	}

}
