/**
 * @brief Algorithme de calcul du plus court chemin (Dijkstra)
 * @details Calcul le plus court chemin pour un bot vers un arret destination dans un graphe donné.
 * 
 * @param graphe le graphe dans lequel se déplace le bot
 * @param bot le bot (position initiale, horaire de départ)
 * @param destination l'arret de destination du bot
 * @return le plus court chemin vers l'arret destination depuis la position initiale du bot
 */
List<Arret> calculParcourDijkstra(Graphe graphe, Bot bot, Arret destination){
// - INITIALISATION
	// On marque tous les arrets comme non visités
	List<Arret> arretNonVisite = graphe.getArret().values();
	// On marque l'arret courant dans lequel nous sommes
	Arret arretCourant = bot.getArretCourant();
	arretNonVisite.remove(arretCourant);
	// On crée une map pour stocker les temps nécessaires pour effectuer les déplacements
	Map<Arret, Integer> tempsDeplacement = new HashMap<Arret, Integer>();
	// On crée une map pour stocker les parcours associés à ces déplacements
	Map<Arret, List<Arret>> parcour = new HashMap<Arret, List<Arret>>();

// - BOUCLE DE CALCUL
	// Tant qu'il reste des arrets non visités
	while(arretNonVisite.size() != 0){
		// - ACTUALISATION DES DISTANCES
		// On constitue la collection des arrets successeurs
		Map<Arret, Integer> arretsSuivant = constituerListeArretSuivant(graphe, arretCourant);
		// Pour chaque arret successeur de l'arret courant 
		for (Arret a : arretsSuivant){
			// On calcule le temps de déplacement vers a
			int tempsDeplacementVersA = (calculerTempsDeplacement(arretsSuivant.get(a), bot.getDate(), arretCourant, a) + tempsDeplacement.get(arretCourant));
			// Si passer par l'arret courant est plus court que le parcour retenu jusqu'ici
			if (tempsDeplacementVersA < tempsDeplacement.get(a)){
				// On modifie le parcour vers a dans la mémoire
				tempsDeplacement.put(a, tempsDeplacementVersA);
				parcour.put(a, parcour.get(arretCourant).add(a));
			}
		}
		// - SELECTION DES ETATS
		// On choisi l'arret n'étant pas visité qui a la plus petite distance
		if (arretNonVisite.size() != 0){
			// On crée des variables pour retenir en mémoire l'arret le plus rapide trouvé pour l'instant
			Arret arretRapide;
			int tempsParcour = 200;
			// On cherche l'arret qui a la plus petite distance dans les arrets non visités
			for (Arret a : arretNonVisite){
				if (tempsDeplacement.get(a) < tempsParcour){
					arretRapide = a;
					tempsParcour = tempsDeplacement.get(a);
				}
			}
			// On sélectionne cet arret comme arret courant pour la suite de l'algorithme
			arretCourant = arretRapide;
		}	
	}
	// On retourne le parcour le plus cours vers l'arret de destination
	return parcour.get(destination);
}

/**
 * @brief Constitution dynamique de la liste des successeurs d'un arret et les lignes par lesquelles les arrets sont reliés
 * @details Constitue la liste des successeurs d'un arret en concaténant l'ensemble des arrets des lignes de l'arret de départ
 * 
 * @param graphe le graphe (lignes et arrets) sur lequel on travail
 * @param arret l'arret duquel on veut créer la liste de successeurs
 * 
 * @return un dictionnaire qui contient les arrets successeurs ainsi que l'id de la ligne qui relie chaque arret à son prédecesseur
 */
Map<Arret, Integer> constituerListeArretSuivant(Graphe graphe, Arret arret){
	// On calcul la capacité maximale de notre liste d'array
	int tailleListe = 0;
	for (int ligne : arret.getLigne())
		tailleListe += graphe.getLigne().get(ligne).getArrets().size();
	// On crée une liste de la taille voulue
	List<Arret> arretSuivants = new ArrayList(tailleListe);
	// Pour chaque ligne auxquelles appartient l'arret
	for (int ligne : arret.getLigne()){
		// On ajoute les arrets qui ne sont pas encore dans notre liste d'arrets successeurs et on note qu'il sont reliés par la ligne
		for (Arret a : graphe.getLigne().get(ligne).getArrets()){
			if (!arretSuivants.contains(a))
				arretSuivants.put(a, ligne);
		}
	}
	// On retourne finalement la liste constituée
	return arretSuivants;
}

/**
 * @brief [brief description]
 * @details [long description]
 * 
 * @param graphe [description]
 * @param e [description]
 * @param arretDepart [description]
 * @param arretDestination [description]
 * @return [description]
 */
int calculerTempsDeplacement(int ligne, String date, Arret arretDepart, Arret arretDestination){
	try {
		// On récupère la liste des horaires des deux arrets
		List<Integer> horairesDepart = arretDepart.getHoraire().get(ligne).get(jour);
		List<Integer> horairesDestination = arretDestination.getHoraire().get(ligne).get(jour);
		// On converti l'horaire du bot au format voulu
		horaireDepartBot = convertIntoMinutes(date);
		// On cherche le premier horaire qui coincide au départ
		for (int i = 0; i < horairesDepart.size(); i++){
			if (horairesDepart.get(i) >= horaireDepartBot)
				return horairesDestination.get(i) - horaireDepartBot;
		}
		// Si il n'y en a pas, on retourne + l'infini
		return Integer.MAX_VALUE;
	}
	// Si aucun horaire n'est disponnible, on renvoie + l'infini
	catch(Exception e){
		return Integer.MAX_VALUE;
	}
}

// Algo de mike keith
int calculerJourSemaine( int annee, int mois, int jours){
	if (mois < 3) annee--;
	return  (((23 * mois)/9) + jours + 4 + annee + (annee/4) - (annee/100) + (annee/400) - 2) % 7;
}