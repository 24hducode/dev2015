package uf;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import utils.IO;

public class GenerateMap {
	
	private Gson gson;
	private List<RawData> rawDataList;
	// [<ligne, <arret1, arret2>>]
	private List<Entry<String, Entry<String, String>>> finalRoutes;
	// {id arret: nom arret}
	private Map<String, String> finalStops;
	// {id ligne: couleur}
	private Map<String, String> finalTracks;
	
	/**
	 * Constructeur
	 * @param folderPath	chemin du dossier contenant les fichiers json
	 */
	public GenerateMap(String folderPath) {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		this.rawDataList = this.readAllJsonFileFrom(folderPath);
		this.finalRoutes = new ArrayList<Entry<String, Entry<String, String>>>();
		this.finalStops = new HashMap<String, String>();
		this.finalTracks = new HashMap<String, String>();
	}
	
	/**
	 * Construit les RawData de chaque fichier json du dossier spécifié
	 * @param folderPath	chemin du dossier contenant les fichiers json
	 * @return	liste de RawData correspondant à chaque fichier json
	 */
	private List<RawData> readAllJsonFileFrom(String folderPath) {
		File folder = new File(folderPath);
		List<RawData> rdList = new ArrayList<RawData>();
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory()) {
				String filename = fileEntry.getName();
				String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
				if (extension.equalsIgnoreCase("json")) {
					rdList.add(this.readJsonFile(fileEntry.getPath()));
				}
			}
		}
		return rdList;
	}
	
	/**
	 * Créé un RawData à partir du chemin d'un fichier json
	 * @param path	chemin du fichier json
	 * @return	RawData correspondant créé
	 */
	private RawData readJsonFile(String path) {
		String rawdata = IO.readTextFileAsString(path);
		return gson.fromJson(rawdata, RawData.class);
	}
	
	/**
	 * Méthode permettant de générer un fichier dot contenant le graphe construit à partir des fichiers json du dossier spécifié dans le constructeur
	 *
	 * @param filename	nom du fichier à générer
	 */
	public void toDotFile(String filename) {
		
		StringBuilder sb = new StringBuilder();
		
		// Structure de base
		sb.append("graph Map {\n");
		sb.append("\tgraph [dpi = 150]\n"); 
		sb.append("\tnodesep=0.3;\n");
		sb.append("\tranksep=0.2;\n");
		sb.append("\tmargin=0.1;\n");
		sb.append("\tnode [shape=circle];\n");

		// Ajout des relations (arc) pour chaque ligne
		for (RawData rd : this.rawDataList) {
			System.out.println("Generate track : "+rd.getTrack_number());
			
			String color;
			do {
				color = getRandomColor();
			}while (this.finalTracks.values().contains(color));
			
			finalTracks.put(rd.getTrack_number(), color);
			sb.append("\tedge [penwidth=5, color=\""+color+"\"];\n");
			
			List<Map<String, String>> stops = rd.getStops();
			for (int i = 1; i < stops.size()-1; i++) {
				// Si le lien entre deux arrêts n'existe pas, on l'ajoute l'ensemble
				if ((!this.hasStop(rd.getTrack_id(), stops.get(i-1).get("id"), stops.get(i).get("id")))
					&& (!this.hasStop(rd.getTrack_id(), stops.get(i).get("id"), stops.get(i+1).get("id")))
					) {
					sb.append("\t"+stops.get(i-1).get("id")+" -- "+stops.get(i).get("id")+";\n");
					sb.append("\t"+stops.get(i).get("id")+" -- "+stops.get(i+1).get("id")+";\n");
					
					finalRoutes.add(new AbstractMap.SimpleEntry<String, Entry<String, String>>(rd.getTrack_id(), new AbstractMap.SimpleEntry<String, String>(stops.get(i-1).get("id"), stops.get(i).get("id"))));
					finalRoutes.add(new AbstractMap.SimpleEntry<String, Entry<String, String>>(rd.getTrack_id(), new AbstractMap.SimpleEntry<String, String>(stops.get(i+1).get("id"), stops.get(i).get("id"))));
				}
				
				// On ajoute l'arrêt à la liste des arrêts finaux si il n'y est pas
				if (!finalStops.containsKey(stops.get(i-1).get("id"))) {
					finalStops.put(stops.get(i-1).get("id"), stops.get(i-1).get("name"));
				}
			}
		}
		
		// Ajout des labels pour chaque arrêt
		for (Entry<String, String> finalStop : this.finalStops.entrySet()) {
			sb.append("\t"+finalStop.getKey()+" [label=\""+finalStop.getValue()+"\n n°"+finalStop.getKey()+"\"];\n");
		}
		
		// Création de la légende
		sb.append("\tsubgraph cluster_01 { \n");
		sb.append("\t\tlabel = \"Légende\"; \n");
		sb.append("\t\tpos = \"1,1!\"; \n");
		sb.append("\t\tedge [arrowsize=0, penwidth=3]; \n");
		sb.append("\t\tkey [shape=none, margin=0, label=<<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" cellborder=\"0\"> \n");
		
		for (Entry<String, String> finalTrack : this.finalTracks.entrySet()) {
			sb.append("\t\t\t<tr><td>"+finalTrack.getKey()+"</td><td bgcolor=\""+finalTrack.getValue()+"\"></td></tr> \n");
		}
		
		sb.append("\t\t</table>>] \n");
		sb.append("\t} \n");
		
		sb.append("}");
		
		// Ecriture dans le fichier
		try {
			FileWriter fstream = new FileWriter("data/"+filename+".dot");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(sb.toString());
			out.close();
		}catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * Méthode permettant de savoir si l'ensemble des données finaux contient un lien entre deux arrêts pour une ligne.
	 * Cette méthode permet d'éviter la duplication de relations identiques entre deux noeuds dans le graphe.
	 * 
	 * @param trackID	identifiant de la ligne
	 * @param stopID1	identifiant du premier arrêt
	 * @param stopID2	identifiant du second arrêt
	 * @return	true si la relation stopID1 -> stopID2 (ou inversement) existe pour cette ligne, false sinon
	 */
	public boolean hasStop(String trackID, String stopID1, String stopID2) {
		boolean has = false;
		
		for (Entry<String, Entry<String, String>> track : this.finalRoutes) {
			if (track.getKey().equals(trackID)) {
				Entry<String, String> finalRoute = track.getValue();
				if (finalRoute.getKey().equals(stopID1) && finalRoute.getValue().equals(stopID2) ||
					finalRoute.getKey().equals(stopID2) && finalRoute.getValue().equals(stopID1)) {
					has = true;
				}
			}
		}
		
		return has;
	}
	
	/**
	 * Génère un code de couleur aléatoirement
	 * @return
	 */
	public static String getRandomColor() {
        String[] letters = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String color = "#";
        for (int i = 0; i < 6; i++ ) {
           color += letters[(int) Math.round(Math.random() * 15)];
        }
        return color;
	}
	
	public static void main(String[] args) {
		GenerateMap gm = new GenerateMap("data/data_files/");
		gm.toDotFile("map");
	}
}