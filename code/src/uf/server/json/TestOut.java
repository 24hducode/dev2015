/*


{
    'success': True,
    'time': 0,
    'status':'game_started',
    'message':'The game has started !',
    'first_stop': {'id': id_du_depart, 'name': nom_du_depart},
    'target': {'id': id_de_l_arrivee, 'name': nom_de_l_arrivee},
    'dtstart': "YYYY-MM-DD "
    'url':  '/api/play/<game_token>/<bot_token>/move'
}


 */

package uf.server.json;

import java.util.Map;

public class TestOut{
	private String success;
	private String time;
	private String status;
	private String message;
	private Map<String,String> first_stop;
	private Map<String,String> target;
	private String dtstart;
	private String url;
	
	public TestOut(String success, String time, String status, String message,
			Map<String,String> first_stop, Map<String,String> target, String dtstart, String url) {
		super();
		this.success = success;
		this.time = time;
		this.status = status;
		this.message = message;
		this.first_stop = first_stop;
		this.target = target;
		this.dtstart = dtstart;
		this.url = url;
	}

	public boolean isSuccess() {
		return Boolean.parseBoolean(success);
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public int getTime() {
		return Integer.parseInt(time);
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String,String> getFirst_stop() {
		return first_stop;
	}

	public void setFirst_stop(Map<String,String> first_stop) {
		this.first_stop = first_stop;
	}

	public Map<String,String> getTarget() {
		return target;
	}

	public void setTarget(Map<String,String> target) {
		this.target = target;
	}

	public String getDtstart() {
		return dtstart;
	}

	public void setDtstart(String dtstart) {
		this.dtstart = dtstart;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}