public class Bot {
	private int tempsCourant;
	private Arret arretCourant;
	public int getTempsCourant() {
		return tempsCourant;
	}
	public void setTempsCourant(int tempsCourant) {
		this.tempsCourant = tempsCourant;
	}
	public Arret getArretCourant() {
		return arretCourant;
	}
	public void setArretCourant(Arret arretCourant) {
		this.arretCourant = arretCourant;
	}
}
