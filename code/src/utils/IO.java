package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class IO {

	/**
	 * http://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
	 * Alternative exists: faster but more memory consuming
	 */
	public static List<String> readTextFileAsStringList( String filePath ) {
		BufferedReader reader;
		List<String> lineList = new ArrayList<String>();
		try {
			reader = new BufferedReader( new FileReader (filePath));

			String         line = null;
			//StringBuilder  stringBuilder = new StringBuilder();
			//String         ls = System.getProperty("line.separator");

			while( ( line = reader.readLine() ) != null ) {
				//stringBuilder.append( line );
				//stringBuilder.append( ls );
				lineList.add(line);

			} 
			reader.close();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lineList;
		//return stringBuilder.toString();
	}

	/**
	 * http://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
	 * Alternative exists: Slower but less memory consuming
	 */
	public static String readTextFileAsString(String path){

		FileInputStream stream = null;
		try {
			File f = new File(path);
			if (!f.exists()) { 

				System.err.println("Error: the path >"+path+"< is not correct. Please check it");
				System.exit(1);
			}
			stream = new FileInputStream(f);
			stream.close();
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//System.err.println("Info: loading text data from >"+path+"<");

		BufferedReader reader = null;
		try {
			reader = new BufferedReader( new FileReader (path));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		StringBuilder builder = new StringBuilder();
		String aux = "";
		try {
			while ((aux = reader.readLine()) != null) {
				builder.append(aux);builder.append("\n");
			}
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return builder.toString();
	}
	
	
	/**
	 * print a string to the file system
	 * @param text
	 * @param filename
	 * @param append TODO
	 */
	public static void writeFile (String text, String filename, Boolean append) {
		PrintWriter out = null;

		int dirPathEnd = filename.lastIndexOf(File.separator);
		String dirPath = "";
		if (dirPathEnd != -1) {
			dirPath = filename.substring(0, dirPathEnd);
			createDir(dirPath); 
		}
		try {
			out = new PrintWriter(new FileOutputStream(
					new File(filename), 
					append));
			//			out = new PrintWriter(filename);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.print(text);
		out.close();
	}

	/**
	 * Create directory
	 * @param dirPath
	 * @return
	 */
	public static void createDir(String dirPath) {

		File theDir = new File(dirPath);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.err.println("Info: creating directory: " + dirPath);
			boolean result = theDir.mkdir();  

			if(!result) {    
				System.err.println("Error: problem when creating directory: " + dirPath);  
			}
		}
	}
}
