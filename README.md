# 24h du Code (au Mans) - Edition 2015 - Team IndexOutOfRange

Voici le code que nous avons produit lors du concours des 24h du code le week-end du 17-18 Janvier 2015.

## À propos du code

Le code que nous avons produit pourrait être revu pour améliorer sa qualité. À cause des contraintes de temps (24h) nous avons préféré nous focaliser sur la partie fonctionnelle plutôt au détriment de la partie documentation.

### Technologies

Nous avons utilisé le langage Java et l'algorithme Dijkstra pour le path finding dans le graphe. Nous nous sommes servis des librairies GSON de Google et HTTPClient d'Apache.

### Plan du réseau

Nous avons également généré un plan du réseau de transport en commun (cf. code/data/map.pdf). Nous avons pour cela utilisé le langage DOT (faisant parti des outils Graphviz) pour construire un graphe visuel. Un code couleur et une légende associée ont été ajouté pour une meilleure lecture. 

## À propos du sujet

Quatre sujets différents sont disponibles au début des 24h. Nous avons choisi le sujet : Urban Flow : "opération de secours" qui a été créé par l'association HAUM ([http://www.haum.org/](http://www.haum.org/)).

Les informations sur le sujet sont disponible à cette adresse [http://haum.org/24hc15/index.html](http://haum.org/24hc15/index.html)

L'énoncé du sujet est le suivant : Ton objectif si tu acceptes, est de participer en tant qu’agent de la SETRAM à une opération de sauvetage après la perdition de trois des membres du collectif “Urban Flow” qui se sont égarés dans le réseau de transport en commun du Mans.

Ce collectif milite pour l’ouverture des données de transports et ces derniers se sont lancé le défi insensé de cartographier le réseau de transports en commun du Mans (i.e. les lignes, les correspondances, et les horaires) avec l’objectif d’en faire un jeu de donnée open-data.

Bien décidés à aller au bout mais sous équipés, ce sont leurs camarades d’Urban Flow qui ont donné l’alerte, ne les revoyant pas revenir.

Alors que deux membres sont toujours en cours de localisation, la troisième personne a elle été localisée. Ta mission est donc d’aller à sa rencontre pour la ramener.

## Remerciements
Nous remercions chaleureusement les organisateurs ainsi que les membres de l'association HAUM (toujours près à nous apporter leur aide) qui nous ont permis de vivre ce très bon moment dans les locaux de la CCI du Mans.

## Auteurs
Remi L., Argann B., Damien R., Kilian B., Clément G.

## Licence
MIT Licence
