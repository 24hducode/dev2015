package uf;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

import utils.IO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PureData {

	private Gson gson;
	public List<RawData> rawDataList;
	private Map<Integer, Arret> listeArrets;
	private Map<Integer, Ligne> lignes;

	/**
	 * Constructeur
	 * @param folderPath le dossier contenant les fichiers json des lignes
	 */
	public PureData(String folderPath) {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		this.rawDataList = this.readAllJsonFileFrom(folderPath);
		this.getListeArrets();
		this.getListeLignes();
	}

	/**
	 * Construit les RawData de chaque fichier json du dossier spécifié
	 * @param folderPath
	 * @return	une liste de RawData
	 */
	private List<RawData> readAllJsonFileFrom(String folderPath) {
		File folder = new File(folderPath);
		List<RawData> rdList = new ArrayList<RawData>();
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.isDirectory()) {
				String filename = fileEntry.getName();
				String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
				if (extension.equalsIgnoreCase("json")) {
					rdList.add(this.readJsonFile(fileEntry.getPath()));
				}
			}
		}
		return rdList;
	}

	/**
	 * Créé un RawData à partir du chemin d'un fichier json
	 * @param path
	 * @return
	 */
	private RawData readJsonFile(String path) {
		String rawdata = IO.readTextFileAsString(path);
		RawData test = gson.fromJson(rawdata, RawData.class);
		return test;
	}

	public Graphe convertToGraph(){
		return new Graphe(this.lignes, this.listeArrets);
	}

	public Map<Integer, Arret> getListeArrets(){
		//On crée une map id_arret -> Arret
		Map<Integer, Arret> arrets = new HashMap<Integer, Arret>();
		//On parcours tout les rawdata
		for(RawData rawdata : this.rawDataList){
			//On récupère l'id de la ligne en cours
			int idLigne = Integer.parseInt(rawdata.getTrack_id());
			//On choppe tout les arrets de la ligne
			List<Map<String, String>> arretsLigne = rawdata.getStops();
			//On parcours tout les arrets de la ligne
			for(Map<String, String> arret : arretsLigne){
				//On crée les nouveaux horaires sous forme d'integer
				Map<Integer, List<Integer>> newHoraires = new HashMap<Integer, List<Integer>>();
				int numJour;
				//On parcours les horaires pour chaque jour
				if (rawdata.getSchedule().get(arret.get("id")) == null) {
					newHoraires = null;
				} else {
					for (Entry<String, List<String>> horaireJour : rawdata.getSchedule().get(arret.get("id")).entrySet()){
						//On enregistre le numéro du jour dans numJour
						numJour = this.convertDaytoInt(horaireJour.getKey());
						//On converti ensuite chaque heure par un temps en minutes
						List<Integer> horairesInt = new ArrayList<Integer>();
						for (String minutesStr : horaireJour.getValue()){
							horairesInt.add(this.convertIntoMinute(minutesStr));
						}
						//Et on met tout ça dans newHoraires
						newHoraires.put(numJour, horairesInt);
					}
				}

				if (arrets.containsKey(Integer.parseInt(arret.get("id")))){
					arrets.get(Integer.parseInt(arret.get("id"))).addLigne(idLigne);
					arrets.get(Integer.parseInt(arret.get("id"))).addHoraire(idLigne, newHoraires);
				} else {
					List<Integer> lignes = new ArrayList<Integer>();
					lignes.add(idLigne);
					Map<Integer, Map<Integer, List<Integer>>> horairesBon = new HashMap<Integer, Map<Integer, List<Integer>>>();
					horairesBon.put(idLigne, newHoraires);
					arrets.put(Integer.parseInt(arret.get("id")), new Arret(lignes,
														  arret.get("name"),
														  Integer.parseInt(arret.get("id")),
														  horairesBon));
				}
			}
			// System.out.println("Fin de la ligne : "+idLigne+", nbr arrets : "+arretsLigne.size());
		}
		this.listeArrets = arrets;
		return this.listeArrets;
	}

	public Map<Integer, Ligne> getListeLignes(){
		List<Arret> arrets = null;
		Map<Integer, Ligne> lignes = new HashMap<Integer, Ligne>();
		//On parcours tout les rawdata
		for(RawData rawdata : this.rawDataList){
			arrets = new ArrayList<Arret>();
			int idLigne = Integer.parseInt(rawdata.getTrack_id());
			String nomLigne = rawdata.getTrack_number();
			int nbrArrets = rawdata.getStops().size();
			for (int i = 0; i < nbrArrets; i++){
				arrets.add(i, null);
			}
			for (Map<String, String> arretsLigne : rawdata.getStops()){

				int idArret_int = Integer.parseInt(arretsLigne.get("id"));
				arrets.set(Integer.parseInt(arretsLigne.get("position"))-1, this.listeArrets.get(idArret_int));
			}
			lignes.put(idLigne, new Ligne(arrets, idLigne, nomLigne));
		}
		this.lignes = lignes;
		return lignes;
	}


    public int convertDaytoInt(String day){
        int jour = 0;
        if (day.equals("lu")){
            jour = 1;
        }else if (day.equals("ma")){
            jour = 2;
        }else if (day.equals("me")){
            jour = 3;
        }else if (day.equals("je")){
            jour = 4;
        }else if (day.equals("ve")){
            jour = 5;
        }else if (day.equals("sa")){
            jour = 6;
        }else if (day.equals("di")){
            jour = 0;
        }else{
            jour = 101;
        }
        return jour;
    }

	public int convertIntoMinute(String iso8601){
		String[] heureSplit = iso8601.split(":");
		Integer h = Integer.parseInt(heureSplit[0]);
		Integer m = Integer.parseInt(heureSplit[1]);
		int result;
		result = h*60+m;
		return result;
	}

	public int calculerJourSemaine(int annee, int mois, int jours){
		if (mois < 3) annee--;
		return  (((23 * mois)/9) + jours + 4 + annee + (annee/4) - (annee/100) + (annee/400) - 2) % 7;
	}
}
