/*{
    "secret_token": <bot_secret>,
    "track": <track_number>,
    "connection": datetime_str, // forme : 13 Sep 13:20:00 2015
    "to_stop": stop_id,
    "type": "move"
}*/

package uf.server.json;

public class MoveIn {
	private String secret_token;
	private String track;
	private String connection;
	private int to_stop;
	private String type;

	public MoveIn(String secret_token, String track, String connection, int to_stop, String type) {
		super();
		this.secret_token = secret_token;
		this.track = track;
		this.connection = connection;
		this.to_stop = to_stop;
		this.type = type;
	}

	public MoveIn() {

	}

	public String getSecret_token() {
		return secret_token;
	}

	public void setSecret_token(String secret_token) {
		this.secret_token = secret_token;
	}
	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getConnection() {
		return connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	public int getTo_stop() {
		return to_stop;
	}

	public void setTo_stop(int to_stop) {
		this.to_stop = to_stop;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}