import java.util.List;
import java.util.Map;

public class Arret {
	private List<Ligne> ligne;
	private String nom;
	private int idArret;
	private Map<Integer, Map<Integer, List<Integer>>> horaire;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getIdArret() {
		return idArret;
	}
	public void setIdArret(int idArret) {
		this.idArret = idArret;
	}
	
	public List<Ligne> getLigne() {
		return ligne;
	}
	public void setLigne(List<Ligne> ligne) {
		this.ligne = ligne;
	}
	public Map<Integer, Map<Integer, List<Integer>>> getHoraire() {
		return horaire;
	}
	public void setHoraire(Map<Integer, Map<Integer, List<Integer>>> horaire) {
		this.horaire = horaire;
	}
	
	
}
