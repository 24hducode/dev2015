package uf;

import javax.rmi.CORBA.Util;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Utils {

	public static int calculerJourSemaine(String date){
		// Format attendu : 2015-01-17
		if (!date.matches("^\\d{4}-\\d{2}-\\d{2}$")){
			System.out.println("ERREUR : La date entrée " + date + " ne correspond pas aui pattern ^\\d{4}-\\d{2}-\\d{2}$");
			// Remplacer par une exception
		}
		String[] dateSplited = date.split("-");
		int annee = Integer.parseInt(dateSplited[0]);
		int mois = Integer.parseInt(dateSplited[1]);
		int jours = Integer.parseInt(dateSplited[2]);
		return Utils.calculerJourSemaine(annee, mois, jours);
	}

	public static int calculerJourSemaine(int annee, int mois, int jours){
		if (mois < 3) annee--;
		return  (((23 * mois)/9) + jours + 4 + annee + (annee/4) - (annee/100) + (annee/400) - 2) % 7;
	}


	public static String convertIntoISO(int minute, String dateInitial){

		// Vérification format
		if (dateInitial.indexOf("+") != -1) {
			// System.out.println(dateInitial.split("\\+")[0]);
			dateInitial = dateInitial.split("\\+")[0] + "Z";
		}
		if (dateInitial.indexOf("T") != -1) {
			// System.out.println(dateInitial.split("\\+")[0]);
			dateInitial = dateInitial.split("T")[0];
		}

		int heure = minute / 60;
		minute = minute % 60;
		String[] dateSplit = dateInitial.split("-");
		int jour = Integer.parseInt(dateSplit[2]);
		jour += heure / 24;
		heure = heure % 24;
		//String date = dateSplit[0] + "-" + dateSplit[1] + "-" + jour;
		String date = "" + jour;
		String mois[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		date += " "+mois[Integer.parseInt(dateSplit[1])]+" ";
		date += heure + ":" + minute + ":00 " + dateSplit[0];
		return date;
	}

	public static String convertIntoISO2(int minute, String dateInitial){
		int heure = minute / 60;
		minute = minute % 60;
		String[] dateSplit = dateInitial.split("-");
		int jour = Integer.parseInt(dateSplit[2]);
		jour += heure / 24;
		heure = heure % 24;
		String date = dateSplit[0] + "-" + dateSplit[1] + "-" + jour;
		return date + "T" + heure + ":" + minute + ":00+00:00";
	}

	public static int[] convertIntoMinute(String iso8601){
		//2015-01-17T16:56:00Z
		if (!iso8601.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(Z|\\+\\d{2}:\\d{2})$")){
			System.out.println("ERREUR : La date entrée " + iso8601 + " ne correspond pas au pattern ^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$");
			// Remplacer par une exception
		}
		String[] part = iso8601.split("T");
		String date = part[0];
		String heure = part[1];
		String[] dateSplit = date.split("-");
		int jourSemaine = calculerJourSemaine(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]));
		String[] heureSplit = heure.split(":");
		Integer h = Integer.parseInt(heureSplit[0]);
		Integer m = Integer.parseInt(heureSplit[1]);
		int[] result = new int[2];
		result[0] = jourSemaine;
		result[1] = h*60+m;
		return result;
	}
}