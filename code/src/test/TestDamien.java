package test;

import java.io.*;
import java.util.*;

import uf.server.json.AskOut;
import uf.server.json.MoveOut;
import uf.server.json.TestOut;
import uf.server.*;
import uf.*;

public class TestDamien {

    public static void main(String[] args) {
        String keyFileName = "keys.json";
        String mode = "eval";

        // String mode = "arena";
        int botNumber = 1;
        int nombreDeTestMax = 30;
        int time; // secondes

        InteractionServer is = new InteractionServer(keyFileName);

        // Init game
        AskOut askOut = is.ask4Game(botNumber, mode);

        if (!askOut.getStatus().equals("registered")) {
            error("registered");
        }

        System.out.println(askOut.getMessage());
        System.out.println(/* Retour à la ligne */);

        /*
        ================================================================
        ===============    Vérifier si le jeu à démarré    =============
        ================================================================
         */

        TestOut testStarted = null;
        int nb = 0;
        do{
            testStarted = is.testStarted(askOut.getUrl(), botNumber);
            if (!testStarted.getStatus().equals("game_started")) {
                nb++;
                System.out.println("[Test debut jeu " + nb +"/" + nombreDeTestMax + "]Sleep 2s : " + testStarted.getMessage());

                // Si pb vérifié si c'est des secondes ou des milisecondes
                int timeRemaning = testStarted.getTime();
                try{
                    Thread.sleep(timeRemaning * 60);
                }catch(Exception e){
                    System.out.println(e);
                }
            }
        }while(!testStarted.getStatus().equals("game_started") && nb < nombreDeTestMax);

        // Message 'The game has started !'
        System.out.println(testStarted.getMessage());
        time = testStarted.getTime();
        //time(time);
        System.out.println(/* Retour à la ligne */);

        /*
        ================================================================
        ===============        Appliquer un mouvement      =============
        ================================================================
         */

        // Calcul du mouvement
        PureData donneeBrute = new PureData("data/data_files/");
        Graphe graphe = donneeBrute.convertToGraph();

        boolean fini = false;

        String urlMove = testStarted.getUrl();
        int currentNumArret = 0;
        String jour = testStarted.getDtstart().split("T")[0];
        String date = jour + "T00:00:00Z";
        int idFirst = Integer.parseInt(testStarted.getFirst_stop().get("id"));
        int idLast  = Integer.parseInt(testStarted.getTarget().get("id"));
        Arret aDep = graphe.getArret().get(idFirst);
        Bot bot = new Bot(testStarted.getDtstart(), aDep);
        System.out.println(bot.getArretCourant());
        Arret target = graphe.getArret().get(idLast);

        Router router = new Router(graphe, bot, target);

        int[] minutes = Utils.convertIntoMinute(testStarted.getDtstart());
        // Itineraire parcours = App.calculerParcoursDijkstra(graphe, bot, graphe.getArret().get(idLast));
        router.calculerParcours_Dijkstra(minutes[1], minutes[0]);
        List<Etape> etapes = router.getEtapes();
        
        int nombreEtapes = etapes.size();
        
        while(!fini){

            // Mauvais Path
            if (etapes == null) {
                error("Parcours NULL");
            }

            // - BOUCLE D'ENVOI AU SERVEUR
            else {
                // Affichage du chemin calculé
                System.out.println("On passe par "+nombreEtapes+" arret(s).");
                for (Etape etape : etapes)
                    System.out.println("On passe par: [" + etape.getArretArrivee() + "] - " + etape.getArretArrivee().getNom());

                // Init loop
                boolean trackTarget = true;
                int score;

                do{
                    System.out.println(bot);
                    // Calcul du coup à envoyer
                    Arret arret = etapes.get(currentNumArret).getArretArrivee();
                    int stopId = arret.getIdArret();
                    String trackId = graphe.getLigne().get(etapes.get(currentNumArret).getLigne()).getNomLigne();
                    // String trackId = parcours.getLigne(parcours.getArret(currentNumArret + 1)).toString();
                    String horaireDePassage = Utils.convertIntoISO(etapes.get(currentNumArret).getHoraireDepart(), jour);

                    // Affichage du coup à envoyer
                    //System.out.println(graphe.getLigne().get(parcours.getLigne(parcours.getArret(currentNumArret + 1))).getNomLigne());
                    System.out.println("MOUVEMENT - trackId : " + trackId + ", stopId : " + stopId + ", horaireDePassage : " + horaireDePassage);

                    // Envoi du coup
                    MoveOut moveOut = is.move(urlMove, botNumber, trackId, stopId, horaireDePassage);
                    System.out.println(moveOut.getStatus() + " - " + moveOut.getMessage());

                    // Si c'�tait le dernier coup, on a fini
                    if(currentNumArret + 1 == nombreEtapes){
                    	fini = true;
                    	trackTarget=false;

                        // Afficher le score
                        System.out.println("Score : " + moveOut.getScore());
                    }
                    
                    // Si le mouvement �tait valide
                    if (moveOut.isSuccess()) {

                    	// Si il n'y a pas de changement d'objectifs
                        if (moveOut.getStatus().equals("moved")) {
                            currentNumArret++; // On avance sur le parcours
                        // Si l'objectif a �t� modifi�
                        }else if(moveOut.getStatus().equals("rerouted")){
                            // On modifie les informations du bot
                            bot.setArretCourant(etapes.get(currentNumArret).getArretArrivee());
                            int horaireArrivee = router.calculHoraireArrivee(etapes.get(currentNumArret).getLigne(), 0, etapes.get(currentNumArret).getHoraireDepart(), etapes.get(currentNumArret - 1).getArretArrivee(), etapes.get(currentNumArret - 1).getArretArrivee());
                            bot.setDate(Utils.convertIntoISO(horaireArrivee, jour));
                            // On modifie la destination
                            router.setTarget(graphe.getArret().get(Integer.parseInt(moveOut.getTarget())));
                            // On recalcul le parcours
                            router.calculerParcours_Dijkstra(bot.getHeureDepart(), bot.getJourSemaine());
                            trackTarget = false;
                        }else{
                            trackTarget = false;
                            fini = true;
                            score = moveOut.getScore();
                        }

                    } else {
                        // Le mouvement n'est pas valide
                        System.out.println("Pénalité +" + moveOut.getPenalty() + "min");
                        time += moveOut.getPenalty();
                        time(time);
                    }

                    System.out.println();
                }while(trackTarget);
            }
        }
    }

    /**
     * Gérer les erreurs
     * @param msg les message d'erreur
     */
    public static void error(String msg){
        System.out.println("ERROR : " + msg);
    }

    /**
     * Afficher le temps
     * @param
     */
    public static void time(int time){
        System.out.println("TIME : " + time);
    }


}