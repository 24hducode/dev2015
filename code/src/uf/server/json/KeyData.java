package uf.server.json;

import java.util.*;

/*
{
    "team_key" : "865017b065a40bb4c4d3bd739a6d092e3f6f830b382d1ef65a4b1e61c0ac5dbc"
    ,"bots" : [
        {
            "nom" : "IndexOutOfBound_bot_0"
            ,"token" : "0e29f38c24e2943adc6da016f182902a"
            ,"secret" : "7a5de225282ea485d05384434314a70ae297b047da395072dda1104fee17fb13"
        }
        ,{
            "nom" : "IndexOutOfBound_bot_1"
            ,"token" : "f7ae9135b948893308cdc0db13b861f0"
            ,"secret" : "bffac0992428028e66c99cf25aa8664c8cc055579d263dad1b6bd2aafe53ed90"
        }
        ,{
            "nom" : "IndexOutOfBound_bot_2"
            ,"token" : "4ce577e145d9acc369f3c366a85b48a7"
            ,"secret" : "b196cb6479f467e2726a77c358bdcb7d9124aef76faff1295997486b8883f386"
        }
    ]
}
 */
public class KeyData {

	public String getTeam_key() {
		return team_key;
	}

	public void setTeam_key(String team_key) {
		this.team_key = team_key;
	}

	public List<KeyBot> getBots() {
		return bots;
	}

	public void setBots(List<KeyBot> bots) {
		this.bots = bots;
	}

	private String team_key;
	private List<KeyBot> bots;

    public String toString(){
        return "{\n\tteam_key : " + this.team_key + "\n\t,bots : " + this.bots.toString() + "}";
    }

}