package uf;

import java.util.*;

public class Router{

	// INPUT DATAS
	private Graphe graphe;
	private Bot bot;
	private Arret destination;
	
	public void setTarget(Arret a){
		this.destination = a;
	}


	// OUTPUT DATAS
	private List<Etape> etapes;


	public Router(Graphe graphe, Bot bot, Arret destination){
		this.graphe = graphe;
		this.bot = bot;
		this.destination = destination;
	}

	public List<Etape> getEtapes() {
		return etapes;
	}

	/**
	 * @brief Algorithme de calcul du plus court chemin (Dijkstra)
	 * @details Calcul le plus court chemin pour un bot vers un arret destination dans un graphe donné.
	 *
	 * @return le plus court chemin vers l'arret destination depuis la position initiale du bot
	 */
	public void calculerParcours_Dijkstra(int tempsCourant, int jourSemaine){
		
		// On initialise la liste des �tapes
		this.etapes = new LinkedList<Etape>();

		// On vérifie que nous ne sommes pas arrivé à destination
		if (this.bot.getArretCourant() == this.destination)
			this.etapes = null;

		// Si nous ne sommes pas arrivé à destination
		else {
			// Creation des variables pour le calcul
			// INPUTS
			Set<Arret> arretsNonVisites = new HashSet(graphe.getArret().values());
			// OUTPUTS
			Map<Arret, Integer> heureArriveeArret = new HashMap<Arret, Integer>();
			for (Arret a : graphe.getArret().values())
				heureArriveeArret.put(a, Integer.MAX_VALUE);
			Map<Arret, Map<Arret, Integer>> heureDepartPourArret = new HashMap<Arret, Map<Arret, Integer>>();
			Map<Arret, Arret> antecedantArret = new HashMap<Arret, Arret>();
			Map<Arret, Integer> ligneVersArret = new HashMap<Arret, Integer>();

			// Initialisation des valeurs
			Arret arretCourant = this.bot.getArretCourant();
			arretsNonVisites.remove(arretCourant);
			antecedantArret.put(arretCourant, null);
			int heureArriveeArretCourant = tempsCourant;
			int jourArriveArretCourant = jourSemaine;

			// Boucle de calcul
			while (arretsNonVisites.size() > 0){
				// Variables temporaires
				Map<Arret, Integer> lignesSuccesseursArretCourant = constituerListeArretSuivant(this.graphe, arretCourant);
				Set<Arret> successeursArretCourant = lignesSuccesseursArretCourant.keySet();
				successeursArretCourant.remove(this.bot.getArretCourant());
				// Initialisation strucuture de donnée des heures de départ pour les différents arrets
				if (!heureDepartPourArret.containsKey(arretCourant))
					heureDepartPourArret.put(arretCourant, new HashMap<Arret, Integer>());

				// Boucle de Dijkstra
				for(Arret arretCourantFor : successeursArretCourant){
					int heureArriveeArretADepuisArretCourant = calculHoraireArrivee(lignesSuccesseursArretCourant.get(arretCourantFor), jourArriveArretCourant, heureArriveeArretCourant, arretCourant, arretCourantFor);
					boolean heureInfereur = heureArriveeArretADepuisArretCourant < heureArriveeArret.get(arretCourantFor);
					if(heureInfereur){
						heureArriveeArret.put(arretCourantFor, heureArriveeArretADepuisArretCourant);
						antecedantArret.put(arretCourantFor, arretCourant);
						ligneVersArret.put(arretCourantFor, lignesSuccesseursArretCourant.get(arretCourantFor));
						int heureDepartPourA = calculPremierHoraireDisponnible(lignesSuccesseursArretCourant.get(arretCourantFor), jourArriveArretCourant, heureArriveeArretCourant, arretCourant)[0];
						heureDepartPourArret.get(arretCourant).put(arretCourantFor, heureDepartPourA);
					}
				}

				// Selection du prochain arret courant
				if (arretsNonVisites.size() > 0){
					heureArriveeArretCourant = Integer.MAX_VALUE;
					for (Arret a : arretsNonVisites){
						
						if (heureArriveeArret.get(a) < heureArriveeArretCourant){
							arretCourant = a;
							heureArriveeArretCourant = heureArriveeArret.get(a);
						}
					}
				}
				
				// Marquage de l'arret comme 'visite'
				arretsNonVisites.remove(arretCourant);
			}
			
			// Reconstitution du parcours vers l'arret de destination
			arretCourant = this.destination;
			while(antecedantArret.get(arretCourant) != null){
				((LinkedList<Etape>) this.etapes).addFirst(new Etape(heureDepartPourArret.get(antecedantArret.get(arretCourant)).get(arretCourant), ligneVersArret.get(arretCourant), arretCourant));
				arretCourant = antecedantArret.get(arretCourant);
			}
			
			for (Etape e : this.etapes){
				System.out.println(e);
			}
		}
	}

	/**
	 * @brief Constitution dynamique de la liste des successeurs d'un arret et les lignes par lesquelles les arrets sont reliÃ©s
	 * @details Constitue la liste des successeurs d'un arret en concatÃ©nant l'ensemble des arrets des lignes de l'arret de dÃ©part
	 *
	 * @param graphe le graphe (lignes et arrets) sur lequel on travail
	 * @param arret l'arret duquel on veut crÃ©er la liste de successeurs
	 *
	 * @return un dictionnaire qui contient les arrets successeurs ainsi que l'id de la ligne qui relie chaque arret Ã  son prÃ©decesseur
	 */
	private Map<Arret, Integer> constituerListeArretSuivant(Graphe graphe, Arret arret){
		// On crÃ©e une liste de la taille voulue
		Map<Arret, Integer> arretSuivants = new HashMap<Arret, Integer>();
		// Pour chaque ligne auxquelles appartient l'arret
		// System.out.println("A"+arret.getIdArret()+" - L:"+arret.getLignes());
		for (int ligne : arret.getLignes()){
			// On ajoute les arrets qui ne sont pas encore dans notre liste d'arrets successeurs et on note qu'il sont reliÃ©s par la ligne
			List<Arret> listeArretLigne = graphe.getLigne().get(ligne).getArret();
			for (Arret a : listeArretLigne){
				if (!arretSuivants.containsKey(a) && (listeArretLigne.indexOf(arret) < listeArretLigne.indexOf(a)))
					arretSuivants.put(a, ligne);
			}
			// System.out.println("\t"+ligne);
		}

		// On retourne finalement la liste constituÃ©e
		return arretSuivants;
	}

	/**
	 * @brief [brief description]
	 * @details [long description]
	 */
	public int calculHoraireArrivee(int ligne, int jourSemaine, int heureArriveeArretDepart, Arret arretDepart, Arret arretDestination){
		try {
			// On récupère la liste des horaires des deux arrets
			int[] horaireDepart = calculPremierHoraireDisponnible(ligne, jourSemaine, heureArriveeArretDepart, arretDepart);
			List<Integer> horairesDestination = arretDestination.getHoraire().get(ligne).get(jourSemaine);
			// On cherche le premier horaire qui coincide au départ
			// En théorie, l'horaire d'arrivée à l'arret de destination correspond à l'horaire de départ du n-eme transport de cet arret
			// n étant le numéro du transport qui nous y emmenne (exemple: le 6e bus de la journée)
			// Pour éviter toute incohérence dans l'algorithme, on vérifie aussi que cet horaire n'est pas antérieur à l'horaire de départ
			for (int i = horaireDepart[1]; i < horairesDestination.size(); i++){
				if (horairesDestination.get(i) > horaireDepart[0])
					return horairesDestination.get(i);
			}
			// Si il n'y en a pas, on retourne + l'infini
			return Integer.MAX_VALUE;
		}
		catch(Exception e){
			return Integer.MAX_VALUE;
		}
	}

	public int[] calculPremierHoraireDisponnible(int ligne, int jourSemaine, int heureArriveeArret, Arret arret){
		try {
			int[] res = new int[2];
			List<Integer> horairesDepart = arret.getHoraire().get(ligne).get(jourSemaine);
			for (int i = 0; i < horairesDepart.size(); i++){
				if (horairesDepart.get(i) >= heureArriveeArret){
					res[0] = horairesDepart.get(i);
					res[1] = i;
					return res;
				}
			}
			res[0] = Integer.MAX_VALUE;
			res[1] = Integer.MAX_VALUE;
			return res;
		}
		catch(Exception e){
			int[] res = new int[2];
			res[0] = Integer.MAX_VALUE;
			res[1] = Integer.MAX_VALUE;
			return res;
		}
	}
}