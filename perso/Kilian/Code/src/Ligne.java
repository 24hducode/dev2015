import java.util.List;

public class Ligne {
	private List<Arret> arret;
	private int idLigne;
	private String nom;
	
	public int getIdLigne() {
		return idLigne;
	}
	public void setIdLigne(int idLigne) {
		this.idLigne = idLigne;
	}
	public String getNomLigne() {
		return nom;
	}
	public void setNomLigne(String nom) {
		this.nom = nom;
	}
	public List<Arret> getArret() {
		return arret;
	}
	public void setArret(List<Arret> arret) {
		this.arret = arret;
	}
	
}
