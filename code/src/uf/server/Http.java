package uf.server;

import com.google.gson.*;
import org.apache.http.client.methods.*;
import org.apache.http.client.*;
import org.apache.http.*;
import org.apache.http.impl.*;
import org.apache.http.impl.client.*;
import org.apache.http.entity.*;
import org.apache.http.message.*;
import org.apache.http.message.*;
import java.io.*;
import java.util.*;


public class Http {

    public static String postJson(String url, String dataToSend){


        InputStream jsonIS = new ByteArrayInputStream(dataToSend.getBytes());

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(url);

        BasicHttpEntity entity = new BasicHttpEntity();
        entity.setContent(jsonIS);
        entity.setContentType(new BasicHeader("Content-type", "application/json"));
        entity.setContentLength(dataToSend.length());
        httppost.setEntity(entity);
        String inputStreamString = null;
        try{
            // System.out.println("POST " + url);
            CloseableHttpResponse response = httpclient.execute(httppost);
            int statusCode = response.getStatusLine().getStatusCode();
            // System.out.println(response);
            if (statusCode > 299 || statusCode < 200)
				throw new Exception("ERREUR SERVEUR ! Code retour : "+statusCode);
            inputStreamString = new Scanner(response.getEntity().getContent(),"UTF-8").useDelimiter("\\A").next();

        }catch(Exception e){
            System.out.println(e);
        }

        return inputStreamString;
    }


}