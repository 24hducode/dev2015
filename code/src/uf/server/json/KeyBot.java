package uf.server.json;

/*
{
    "nom" : "IndexOutOfBound_bot_0"
    ,"token" : "0e29f38c24e2943adc6da016f182902a"
    ,"secret" : "7a5de225282ea485d05384434314a70ae297b047da395072dda1104fee17fb13"
}
 */
public class KeyBot {
	private String nom;
	private String token;
	private String secret;


    public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getSecret() {
		return secret;
	}


	public void setSecret(String secret) {
		this.secret = secret;
	}


	public String toString(){
        return "{\n\tnom : " + this.nom + "\n\ttoken : " + this.token + "\n\tsecret : " + this.secret + "}";
    }

}