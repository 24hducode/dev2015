/*
{
    "secret_token": "<bot_secret>"
}

 */

package uf.server.json;

public class TestIn{
	private String secret_token;

    public TestIn(){

    }

	public TestIn(String secret_token) {
		super();
		this.secret_token = secret_token;
	}

	public String getSecret_token() {
		return secret_token;
	}

	public void setSecret_token(String secret_token) {
		this.secret_token = secret_token;
	}

}
