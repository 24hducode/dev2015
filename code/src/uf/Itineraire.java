package uf;

import java.util.*;

public class Itineraire{
	private Map<Arret, Integer> lignes;
	private List<Arret> arrets;
	private Map<Arret, Integer> horaireArrets;

	public Itineraire(Map<Arret, Integer> lignes, List<Arret> arrets, Map<Arret, Integer> horaireArrets){
		this.lignes = lignes;
		this.arrets = arrets;
		this.horaireArrets = horaireArrets;
	}

	public Integer getLigne(Arret arret){
		return this.lignes.get(arret);
	}

	public Arret getArret(int index){
		return this.arrets.get(index);
	}

	public Integer getHoraire(Arret arret){
		return horaireArrets.get(arret);
	}

	public Integer getNombreChangements(){
		return this.arrets.size();
	}

	public String toString(){
		return "Itineraire - L[" + lignes.toString() + "] - A[" + arrets.toString() + "]";
	}
}