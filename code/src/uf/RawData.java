package uf;
import java.util.List;
import java.util.Map;


public class RawData {
	private String track_id;
	private String track_number;
	private String n_stops;
	private Map<String, String> track_dest;
	private List<Map<String, String>> stops;
	private Map<String, Map<String, List<String>>> schedule;
	/**
	 * @return the track_id
	 */
	public String getTrack_id() {
		return track_id;
	}
	/**
	 * @param track_id the track_id to set
	 */
	public void setTrack_id(String track_id) {
		this.track_id = track_id;
	}
	/**
	 * @return the track_number
	 */
	public String getTrack_number() {
		return track_number;
	}
	/**
	 * @param track_number the track_number to set
	 */
	public void setTrack_number(String track_number) {
		this.track_number = track_number;
	}
	/**
	 * @return the n_stops
	 */
	public String getN_stops() {
		return n_stops;
	}
	/**
	 * @param n_stops the n_stops to set
	 */
	public void setN_stops(String n_stops) {
		this.n_stops = n_stops;
	}
	/**
	 * @return the track_dest
	 */
	public Map<String, String> getTrack_dest() {
		return track_dest;
	}
	/**
	 * @param track_dest the track_dest to set
	 */
	public void setTrack_dest(Map<String, String> track_dest) {
		this.track_dest = track_dest;
	}
	/**
	 * @return the stops
	 */
	public List<Map<String, String>> getStops() {
		return stops;
	}
	/**
	 * @param stops the stops to set
	 */
	public void setStops(List<Map<String, String>> stops) {
		this.stops = stops;
	}
	/**
	 * @return the schedule
	 */
	public Map<String, Map<String, List<String>>> getSchedule() {
		return schedule;
	}
	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(Map<String, Map<String, List<String>>> schedule) {
		this.schedule = schedule;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RawData [track_id=" + track_id + ", track_number="
				+ track_number + ", n_stops=" + n_stops + ", track_dest="
				+ track_dest + ", stops=" + stops + ", schedule=" + schedule
				+ "]";
	}
	/**
	 * @param track_id
	 * @param track_number
	 * @param n_stops
	 * @param track_dest
	 * @param stops
	 * @param schedule
	 */
	public RawData(String track_id, String track_number, String n_stops,
			Map<String, String> track_dest, List<Map<String, String>> stops,
			Map<String, Map<String, List<String>>> schedule) {
		super();
		this.track_id = track_id;
		this.track_number = track_number;
		this.n_stops = n_stops;
		this.track_dest = track_dest;
		this.stops = stops;
		this.schedule = schedule;
	}
	
	
	

}
