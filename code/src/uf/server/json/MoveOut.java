/*

FUSIONNER LES 2 :)


{
    "success": True,
    "status": "rerouted",
    "target': <new_target>,
    "message": "The request was correct, your bot has moved. However it has been rerouted, the target has changed!",
    "stop": {"id": <stop_id>, "name": <stop_name>},
    "time": datetime_str
}



{
    'success': False,
    'status': 'unknown_track',
    'penalty': 30,
    'message':'Sorry, but this track does not exist or you are not allow to take it. Consequently, your bot has been penalized.',
    'stop': {'id': <stop_id>, 'name': <stop_name>},
    'time': datetime_str

}



 */

package uf.server.json;

import java.util.HashMap;
import java.util.Map;

public class MoveOut{
	private String success;
	private String time;
	private String status;
	private String message;
	private Map<String,String> stop;
	private String target;
	private String penalty;
	private String score;

	public MoveOut(String success, String time, String status, String message,
			Map<String,String> stop, String target, String penalty, String score) {
		super();
		this.success = success;
		this.time = time;
		this.status = status;
		this.message = message;
		this.stop = stop;
		this.target = target;
		this.penalty = penalty;
		this.score = score;
	}

	public boolean isSuccess() {
		return Boolean.parseBoolean(success);
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String,String> getStop() {
		return stop;
	}

	public void setStop(Map<String,String> stop) {
		this.stop = stop;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public int getPenalty() {
		return Integer.parseInt(penalty);
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public int getScore() {
		return Integer.parseInt(score);
	}

	public void setScore(String score) {
		this.score = score;
	}

}