package uf;

import java.util.List;


public class Ligne {
	private List<Arret> arrets;
	private int idLigne;
	private String nom;

	/**
	 * @param arret
	 * @param idLigne
	 * @param nom
	 */
	public Ligne(List<Arret> arret, int idLigne, String nom) {
		super();
		this.arrets = arret;
		this.idLigne = idLigne;
		this.nom = nom;
	}

	public int getIdLigne() {
		return idLigne;
	}
	public void setIdLigne(int idLigne) {
		this.idLigne = idLigne;
	}
	public String getNomLigne() {
		return nom;
	}
	public void setNomLigne(String nom) {
		this.nom = nom;
	}
	public List<Arret> getArret() {
		return arrets;
	}
	public void setArret(List<Arret> arret) {
		this.arrets = arret;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Ligne [arrets=" + arrets + ", idLigne=" + idLigne + ", nom="
				+ nom + "]";
	}



}
