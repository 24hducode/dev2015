package uf;

public class Etape{

	public Integer horaireDepart;
	public int ligne;
	public Arret arretArrivee;

	public Etape(Integer horaire, int ligne, Arret arret){
		this.horaireDepart = horaire;
		this.ligne = ligne;
		this.arretArrivee = arret;
	}

	public Integer getHoraireDepart() {
		return horaireDepart;
	}

	public void setHoraireDepart(Integer horaireDepart) {
		this.horaireDepart = horaireDepart;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	@Override
	public String toString() {
		return "Etape [horaireDepart=" + horaireDepart + ", ligne=" + ligne
				+ ", arretArrivee=" + arretArrivee + "]";
	}

	public Arret getArretArrivee() {
		return arretArrivee;
	}

	public void setArretArrivee(Arret arretArrivee) {
		this.arretArrivee = arretArrivee;
	}
}