package uf.server.json;

/**
 * Json récupère informations demande jeu
 * {
 *   'success':True,
 *   'status':'registered',
 *   'message':'Your bot is now registered! Check the url to see when you will play.',
 *   'url': /api/play/<game_token>/<bot_token>/verif
 * }
 */
public class AskOut {
	private boolean success;
	private String status;
	private String message;
	private String url;

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String toString(){
		return "success : " + this.success + "; status : " + this.status + "; message : " + this.message + "; url : " + this.url + ";";
	}
}