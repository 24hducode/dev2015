package test;

import java.util.ArrayList;
import java.util.LinkedList;

import uf.*;

public class TestArgann {

	public static int getTempsTrajetMemeLigne(Arret depart, Arret arrivee, int idLigne){

		int heureDepart = depart.getHoraire().get(idLigne).get(1).get(0);
		int heureArrivee = depart.getHoraire().get(idLigne).get(1).get(0);

		return heureArrivee - heureDepart;
	}

	public static void main(String[] args) {
		PureData test = new PureData("data/data_files/");
		Graphe lol = test.convertToGraph();
		int idLigne = 42;
		ArrayList ltol = (ArrayList) lol.getLigne().get(idLigne).getArret();
		// System.out.println(getTempsTrajetMemeLigne(ltol, idLigne)); //5:34  7:24
	}

}
