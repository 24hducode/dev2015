package uf;
import java.util.Map;

public class Graphe{
	private Map<Integer, Ligne> ligne;
	private Map<Integer, Arret> arret;
	
	public Graphe(Map<Integer, Ligne> ligne,Map<Integer, Arret> arret){
		this.ligne = ligne;
		this.arret = arret;
	}
	
	public Map<Integer, Ligne> getLigne() {
		return ligne;
	}
	public void setLigne(Map<Integer, Ligne> ligne) {
		this.ligne = ligne;
	}
	public Map<Integer, Arret> getArret() {
		return arret;
	}
	public void setArret(Map<Integer, Arret> arret) {
		this.arret = arret;
	}
}
