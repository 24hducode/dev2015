package uf;

import java.util.List;
import java.util.Map;


public class Arret {
	private List<Integer> lignes;
	private String nom;
	private int idArret;
	//          id_ligne     id_jour       nbrMinutes
	private Map<Integer, Map<Integer, List<Integer>>> horaire;



	/**
	 * @param lignes
	 * @param nom
	 * @param idArret
	 * @param horaire
	 */
	public Arret(List<Integer> lignes, String nom, int idArret,
			Map<Integer, Map<Integer, List<Integer>>> horaire) {
		super();
		this.lignes = lignes;
		this.nom = nom;
		this.idArret = idArret;
		this.horaire = horaire;
	}

	public String getNom() {
		return nom;
	}

	public void addLigne(int ligne){
		this.lignes.add(ligne);
	}

	public void addHoraire(Integer ligne, Map<Integer, List<Integer>> horaires){
		this.horaire.put(ligne, horaires);
	}


	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getIdArret() {
		return idArret;
	}
	public void setIdArret(int idArret) {
		this.idArret = idArret;
	}


	public List<Integer> getLignes() {
		return lignes;
	}
	public void setLigne(List<Integer> ligne) {
		this.lignes = ligne;

	}
	public Map<Integer, Map<Integer, List<Integer>>> getHoraire() {
		return horaire;
	}
	public void setHoraire(Map<Integer, Map<Integer, List<Integer>>> horaire) {
		this.horaire = horaire;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "("+ idArret +") - " + nom;
	}
}


